package com.srccodes.example;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

/**
 * Servlet implementation class HelloWorld
 */
@WebServlet("/HelloWorld")
public class HelloWorld extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String MYSQL_USERNAME = "root";
	private static final String MYSQL_PWD = "root";
	private static final String MYSQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/test";
	private static final String MYSQL_CONNECTION_URLDES = "jdbc:mysql://localhost:3306/think1";
	private static final JavaSparkContext sc = new JavaSparkContext(
			new SparkConf().setAppName("Spark Example").setMaster("local[*]"));

	private static final SparkSession sparkSession = SparkSession.builder().master("local").appName("mysql")
			.getOrCreate();   
	private static final SQLContext sqlContext = new SQLContext(sc);
	/**
     * @see HttpServlet#HttpServlet()
     */
    public HelloWorld() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
        PrintWriter printWriter  = response.getWriter();
        printWriter.println("<h1>Hello World!</h1>");
        System.out.println("Hello");
        
        
        
        Properties properties = new Properties();
		properties.put("user", MYSQL_USERNAME);
		properties.put("password", MYSQL_PWD);
		properties.put("driver", "com.mysql.jdbc.Driver");

		

		String tbTables = "(select * from TRANSFORM) as categoty";

		Dataset<Row> entireDB = sparkSession.read().jdbc(MYSQL_CONNECTION_URL, tbTables, properties);
		System.out.println("hello" + entireDB.columns());
		entireDB.printSchema();
		Dataset<String> all = entireDB.toJSON();
		
		List<String> all1 = all.collectAsList();
		//System.out.println(all1);
		sc.parallelize(all1).coalesce(1).saveAsTextFile("hdfs://master:9000/ujjwalweb4");
        
         String bfile = "hdfs://master:9000/ujjwalweb2/part-00000";
         
         Dataset<Row> dataset = sqlContext.read().json(bfile);
         
        
		dataset.write().jdbc(MYSQL_CONNECTION_URLDES, "ujjwalweb1", properties);
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
