package com.bdd.cloudcdc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
 

import com.bdd.cloudcdc.model.ConnectionInfoDTO;
import com.bdd.cloudcdc.model.Employee;
import com.bdd.cloudcdc.model.ProductSupportedDB;
 
@Repository("productSupportedDBDao")
public class ProductSupportedDbDaoImpl extends AbstractDao implements IProductSupportedDBDao{
 
    /*public void saveEmployee(Employee employee) {
        persist(employee);
    }*/
    
    public void saveConnection(ConnectionInfoDTO connectionInfoDTO) {
        persist(connectionInfoDTO);
    }
 
    @SuppressWarnings("unchecked")
    public List<ProductSupportedDB> findAllDBSName() {
        Criteria criteria = getSession().createCriteria(ProductSupportedDB.class);
        return (List<ProductSupportedDB>) criteria.list();
    }
 
    /*public void deleteEmployeeBySsn(String ssn) {
        Query query = getSession().createSQLQuery("delete from Employee where ssn = :ssn");
        query.setString("ssn", ssn);
        query.executeUpdate();
    }
 
     
    public Employee findBySsn(String ssn){
        Criteria criteria = getSession().createCriteria(Employee.class);
        criteria.add(Restrictions.eq("ssn",ssn));
        return (Employee) criteria.uniqueResult();
    }
     
    public void updateEmployee(Employee employee){
        getSession().update(employee);
    }*/
     
}
