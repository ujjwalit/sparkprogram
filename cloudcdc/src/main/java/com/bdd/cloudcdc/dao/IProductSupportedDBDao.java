package com.bdd.cloudcdc.dao;
import java.util.List;

import com.bdd.cloudcdc.model.ConnectionInfoDTO;
import com.bdd.cloudcdc.model.Employee;
import com.bdd.cloudcdc.model.ProductSupportedDB;

public interface IProductSupportedDBDao {
	 
   // void saveEmployee(Employee employee);
	
	void saveConnection(ConnectionInfoDTO connectionInfoDTO);
     
    List<ProductSupportedDB> findAllDBSName();
     
    /*void deleteEmployeeBySsn(String ssn);
     
    Employee findBySsn(String ssn);
     
    void updateEmployee(Employee employee);*/
}
