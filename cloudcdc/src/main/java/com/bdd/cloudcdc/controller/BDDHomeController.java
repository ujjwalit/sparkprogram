package com.bdd.cloudcdc.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bdd.cloudcdc.model.ConnectionInfoDTO;
import com.bdd.cloudcdc.model.ProductSupportedDB;
import com.bdd.cloudcdc.service.EmployeeService;
import com.bdd.cloudcdc.service.IDBListService;
import com.bdd.cloudcdc.utils.JSONUtility;
import com.fasterxml.jackson.core.JsonProcessingException;

@Controller
public class BDDHomeController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private IDBListService iDBListService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(ModelMap model) {

		// System.out.println(employeeService.findAllEmployees().size());

		//System.out.println(iDBListService.findAllDBSName().size());

		/*List<ProductSupportedDB> dbNames = iDBListService.findAllDBSName();
		model.addAttribute("dbNames", dbNames);*/
		return "home";

	}

	/**
	 * Method is used to get Available DataSources
	 * @param model
	 * @return
	 * @throws JsonProcessingException 
	 */
	@RequestMapping(value = "/getavailabledatasource", method = RequestMethod.POST)
	public ResponseEntity<String> getAvailableDataSource(ModelMap model) throws JsonProcessingException {

		// System.out.println(employeeService.findAllEmployees().size());

		//System.out.println(iDBListService.findAllDBSName().size());

		List<ProductSupportedDB> dbNames = iDBListService.findAllDBSName();
		//model.addAttribute("dbNames", dbNames);
		
		String allDB = JSONUtility.changeListToJSON(dbNames);
		//System.out.println(allDB);
		ResponseEntity<String> rs = new ResponseEntity<String>(allDB, HttpStatus.OK);
		
		return rs;
		/*
		 * List dbName = iDBListService.findAllDBSName();
		 * 
		 * for (Iterator iterator = dbName.iterator(); iterator.hasNext();){
		 * ProductSupportedDB productSupportedDB = (ProductSupportedDB)
		 * iterator.next();
		 * 
		 * productSupportedDB.setId(productSupportedDB.getId());
		 * productSupportedDB.setDbkey(productSupportedDB.getDbkey());
		 * productSupportedDB.setDbvalue(productSupportedDB.getDbvalue());
		 * 
		 * model.addAttribute("productSupportedDB", productSupportedDB);
		 * 
		 * System.out.print("ID: " + productSupportedDB.getId());
		 * System.out.print("DBKEY: " + productSupportedDB.getDbkey());
		 * System.out.println("DBVALUE: " + productSupportedDB.getDbvalue()); }
		 */

		//return "home";

	}
	
	@RequestMapping(value = "/savenewconnection", method = RequestMethod.POST)
	public ResponseEntity<String> saveNewConnection(@RequestBody  String connectionInfo) throws IOException {

		
		ConnectionInfoDTO connectionInfoDTO = ConnectionInfoDTO.toJSON(connectionInfo);
		
		System.out.println(connectionInfo);
		iDBListService.saveConnection(connectionInfoDTO);
		// System.out.println(employeeService.findAllEmployees().size());

		ResponseEntity<String> rs = new ResponseEntity<String>("test", HttpStatus.OK);
		
		return rs;
	}

}
