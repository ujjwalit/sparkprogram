package com.bdd.cloudcdc.utils;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONUtility {
	
	
	/**
	 * 
	 * @param obj
	 * @return
	 * @throws JsonProcessingException 
	 */
	public static String changeObjectToJSON(Object obj) throws JsonProcessingException{
		ObjectMapper objectMapper = new ObjectMapper();
		//objectMapper.writeValueAsString(obj);
		return objectMapper.writeValueAsString(obj);
	}
	
	/**
	 * 
	 * @param json
	 * @param element
	 * @return
	 * @throws IOException
	 */
	public static <T> T changeJsonToObject(String json, Class clazz) throws IOException{
		ObjectMapper objectMapper = new ObjectMapper();
		//objectMapper.writeValueAsString(obj);
		return (T) objectMapper.readValue(json, clazz);
	}
	
	/**
	 * 
	 * @param obj
	 * @return
	 * @throws JsonProcessingException
	 */
	public static String changeListToJSON(List<?> list) throws JsonProcessingException{
		ObjectMapper objectMapper = new ObjectMapper();
		//objectMapper.writeValueAsString(obj);
		return objectMapper.writeValueAsString(list);
	}
	
	/*public static void main(String[] args) throws JsonProcessingException {
		ProductSupportedDB db = new ProductSupportedDB();
		db.setDbkey("key");
		db.setDbvalue("value");
		db.setId(20);
		
		ProductSupportedDB db2 = new ProductSupportedDB();
		db2.setDbkey("key");
		db2.setDbvalue("value");
		db2.setId(20);
		
		List<ProductSupportedDB> allProduct = new ArrayList<>();
		allProduct.add(db);
		allProduct.add(db2);
		System.out.println(ObjectToJsonMapper.changeListToJSON(allProduct));
		
	}*/
	

}
