package com.bdd.cloudcdc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 

import com.bdd.cloudcdc.dao.EmployeeDao;
import com.bdd.cloudcdc.dao.IProductSupportedDBDao;
import com.bdd.cloudcdc.model.ConnectionInfoDTO;
import com.bdd.cloudcdc.model.Employee;
import com.bdd.cloudcdc.model.ProductSupportedDB;
 
@Service("idbListService")
@Transactional
public class DBListServiceImpl implements IDBListService{
 
    @Autowired
    private IProductSupportedDBDao dao;
     
   /* public void saveEmployee(Employee employee) {
        dao.saveEmployee(employee);
    }
 */
    public void saveConnection(ConnectionInfoDTO connectionInfoDTO) {
        dao.saveConnection(connectionInfoDTO);
    }
    
    public List<ProductSupportedDB> findAllDBSName() {
        return dao.findAllDBSName();
    }
 
   /* public void deleteEmployeeBySsn(String ssn) {
        dao.deleteEmployeeBySsn(ssn);
    }
 
    public Employee findBySsn(String ssn) {
        return dao.findBySsn(ssn);
    }
 
    public void updateEmployee(Employee employee){
        dao.updateEmployee(employee);
    }*/
}
