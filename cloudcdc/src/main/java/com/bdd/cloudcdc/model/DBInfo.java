/**
 * 
 */
package com.bdd.cloudcdc.model;

/**
 * @author master
 *
 */
public class DBInfo {
	
	private int id;
	
	private String driver;
	
	private int db;

	/**
	 * @return the id
	 */
	public final int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public final void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the driver
	 */
	public final String getDriver() {
		return driver;
	}

	/**
	 * @param driver the driver to set
	 */
	public final void setDriver(String driver) {
		this.driver = driver;
	}

	/**
	 * @return the db
	 */
	public final int getDb() {
		return db;
	}

	/**
	 * @param db the db to set
	 */
	public final void setDb(int db) {
		this.db = db;
	}
	
	

}
