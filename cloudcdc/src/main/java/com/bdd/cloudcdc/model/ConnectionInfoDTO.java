/**
 * 
 */
package com.bdd.cloudcdc.model;

import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author master
 *
 */

@Entity
@Table(name="connectioninfodb")
public class ConnectionInfoDTO {
	
	//private String connName;
	@Id
	@Column(name = "connDesc", nullable = false)
	private String connDesc;
	
	@Column(name = "roleSource", nullable = false)
	private String roleSource;
	
	@Column(name = "roleTarget", nullable = false)
	private String roleTarget;
	
	@Column(name = "connType", nullable = false)
	private int connType;
	/**
	 * @return the connName
	 */
	/*public final String getConnName() {
		return connName;
	}
	*//**
	 * @param connName the connName to set
	 *//*
	public final void setConnName(String connName) {
		this.connName = connName;
	}*/
	/**
	 * @return the connDesc
	 */
	public final String getConnDesc() {
		return connDesc;
	}
	/**
	 * @param connDesc the connDesc to set
	 */
	public final void setConnDesc(String connDesc) {
		this.connDesc = connDesc;
	}
	/**
	 * @return the roleSource
	 */
	public final String getRoleSource() {
		return roleSource;
	}
	/**
	 * @param roleSource the roleSource to set
	 */
	public final void setRoleSource(String roleSource) {
		this.roleSource = roleSource;
	}
	/**
	 * @return the roleTarget
	 */
	public final String getRoleTarget() {
		return roleTarget;
	}
	/**
	 * @param roleTarget the roleTarget to set
	 */
	public final void setRoleTarget(String roleTarget) {
		this.roleTarget = roleTarget;
	}
	/**
	 * @return the connType
	 */
	public final int getConnType() {
		return connType;
	}
	/**
	 * @param connType the connType to set
	 */
	public final void setConnType(int connType) {
		this.connType = connType;
	}
	
	public static ConnectionInfoDTO toJSON(String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, ConnectionInfoDTO.class);
	}
	
	

}
