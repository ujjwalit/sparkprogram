package com.bdd.cloudcdc.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
 
import org.hibernate.annotations.Type;
//import org.joda.time.LocalDate;
 
@Entity
@Table(name="productsupporteddb")
public class ProductSupportedDB {
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
 
    @Column(name = "dbkey", nullable = false)
    private String dbkey;
 
  /*  @Column(name = "JOINING_DATE", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate joiningDate;*/
 
    @Column(name = "dbvalue", nullable = false)
    private String dbvalue;
     
   
    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }

	public String getDbkey() {
		return dbkey;
	}

	public void setDbkey(String dbkey) {
		this.dbkey = dbkey;
	}

	public String getDbvalue() {
		return dbvalue;
	}

	public void setDbvalue(String dbvalue) {
		this.dbvalue = dbvalue;
	}
 
  
 
    
 
    
 
   /* @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
        return result;
    }*/
 
    /*@Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ProductSupportedDB))
            return false;
        ProductSupportedDB other = (ProductSupportedDB) obj;
        if (id != other.id)
            return false;
        if (ssn == null) {
            if (other.ssn != null)
                return false;
        } else if (!ssn.equals(other.ssn))
            return false;
        return true;
    }*/
 
    /*@Override
    public String toString() {
        return "Employee [id=" + id + ", name=" + name + ", joiningDate="
                + "" + ", salary=" + salary + ", ssn=" + ssn + "]";
    }*/
     
     
     
 
}