package com.bdd.cloudcdc.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class BDDAppInitializer implements WebApplicationInitializer {

	
	public void onStartup(ServletContext container) throws ServletException {
		// TODO Auto-generated method stub
		
		 AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
	        applicationContext.register(AppConfig.class);
	        applicationContext.setServletContext(container);
	        
	        
	        ServletRegistration.Dynamic servlet = container.addServlet("dispatcher",
	                        new DispatcherServlet(applicationContext));

	        servlet.setLoadOnStartup(1);
	        servlet.addMapping("/");
		
	}
}
